* Information :htmlonly:
:PROPERTIES:
:CUSTOM_ID: information
:END:

See reproducing guidelines for this study
[[./reproducing-guidelines.org][here]].

* Introduction
:PROPERTIES:
:CUSTOM_ID: introduction
:END:

This is an example experimental study relying on the {{{test_FEMBEM}}} solver
test suite. Here, we are especially interested in solving coupled sparse/dense
FEM/BEM linear systems arising in the domain of aeroacoustics. The idea is to
evaluate the solvers available in the open-source version of {{{test_FEMBEM}}}
for the solution of this kind of linear systems.

* Experimental study
:PROPERTIES:
:CUSTOM_ID: study
:END:

Unfortunately, the open-source version of {{{test_FEMBEM}}} cite:testFEMBEM does
not implement couplings of sparse and dense direct solvers which is normally our
go-to method for solving sparse/dense FEM/BEM systems. Therefore, we rely here
only on dense direct solvers, namely {{{hmat-oss}}} and {{{chameleon}}}.

{{{hmat-oss}}} cite:hmat-oss is an open-source and sequential version of the
compressed hierarchical {{{hmatrix}}} dense direct solver {{{hmat}}} cite:Lize14
developed at Airbus. {{{chameleon}}} cite:chameleon is a fully open-source dense
direct solver without compression.

As of the test case, we consider a simplified {{{spipe}}} which is still close
enough to real-life models (see Figure [[short-pipe]]).

Note that all the benchmarks were conducted on a single call_machine-info().

#+NAME: machine-info
#+HEADER: :exports none :results raw
#+BEGIN_SRC shell
MODEL=$(lscpu | grep "Model name:" | cut -d ':' -f 2 | tr -s ' ' | \
        sed 's/^ //g')
CORES=$(lscpu | grep "Core(s) per socket:" | cut -d ':' -f 2 | tr -s ' ' | \
        sed 's/^ //g')
THREADS=$(lscpu | grep "Thread(s) per core:" | cut -d ':' -f 2 | tr -s ' ' | \
        sed 's/^ //g')
RAM=$(lshw -short -C memory -disable dmi -quiet 2> /dev/null | \
      grep "System memory" | tr -s ' ' | cut -d ' ' -f 3 | sed 's/[^0-9\.]//g')

case $CORES in
  1)
    CORES="mono"
    ;;
  2)
    CORES="bi"
    ;;
  4)
    CORES="quad"
    ;;
  8)
    CORES="octa"
    ;;
  16)
    CORES="hexa"
    ;;
  ,*)
    CORES="$CORES"
    ;;
esac

if test $THREADS -gt 1;
then
  THREADS="with Hyper-Threading"
else
  THREADS="without hyper-threading"
fi

echo "$CORES-core $MODEL machine $THREADS and $RAM GiB of RAM"
#+END_SRC

#+CAPTION: A {{{spipe}}} mesh counting 20,000 vertices.
#+NAME: short-pipe
#+ATTR_LaTeX: :width .5\columnwidth :placement [H]
[[./figures/short-pipe.png]]

** Data compression
:PROPERTIES:
:CUSTOM_ID: compression
:END:

In the first part, we want to know to which extent can data compression improve
the computation time. For this, we compare sequential executions of both
{{{hmat-oss}}}, the compressed solver, and {{{chameleon}}}, the non-compressed
solver, on coupled FEM/BEM systems of different sizes (see Figure
[[hmat-chameleon]]). The results clearly show the advantage of using data
compression, especially with increasing size of the target linear system.

#+CAPTION: Computation times of sequential runs of {{{hmat-oss}}} and
#+CAPTION: {{{chameleon}}} on coupled sparse/dense FEM/BEM linear systems of
#+CAPTION: varying size.
#+NAME: hmat-chameleon
#+ATTR_LaTeX: :width 1\columnwidth
[[./figures/hmat-chameleon.svg]]

For these experiments, we have considered the precision parameter $\epsilon$ for
the {{{hmat-oss}}} solver to be {{{epsilon}}}. In Figure
[[hmat-chameleon-error]], the relative error curve for the runs presented in
Figure [[hmat-chameleon]] verifies that the threshold is respected and that the
error of the solutions computed by {{{hmat-oss}}} is even smaller than
$\epsilon$.

#+CAPTION: Relative error of sequential runs of {{{hmat-oss}}} and
#+CAPTION: {{{chameleon}}} on coupled sparse/dense FEM/BEM linear systems of
#+CAPTION: varying size.
#+NAME: hmat-chameleon-error
#+ATTR_LaTeX: :width 1\columnwidth
[[./figures/hmat-chameleon-error.svg]]

** Multi-threaded execution
:PROPERTIES:
:CUSTOM_ID: parallel
:END:

To study the impact of parallel execution on the time to solution, we limit
ourselves to the {{{chameleon}}} solver as {{{hmat-oss}}} is sequential-only. In
Figure [[chameleon]], we comapre the computation times of {{{chameleon}}} on
coupled FEM/BEM systems of different sizes using either one or four threads.
According to the results, we can observe a significant decrease in computation
time in case of parallel executions. Moreover the parallel efficiency of the run
on the largest linear system considered (8000 unknowns) is approximately
call_efficiency(results="benchmarks/results/results.csv", size=8000, nt=4)%.

#+NAME: efficiency
#+HEADER: :var results="hello" size=-1 nt=-1 :exports none :results raw
#+BEGIN_SRC R
data <- read.csv(file = results, header = TRUE)
data <- subset(data, solver == "chameleon" & nbpts == size)
data$time <- data$tps_facto + data$tps_solve
Ts <- data[data$threads == 1, "time"]
Tp <- data[data$threads == nt, "time"]
E <- (Ts / (Tp * nt)) * 100 
print(as.integer(E))
#+END_SRC

#+CAPTION: Computation times of sequential and parallel runs of {{{chameleon}}}
#+CAPTION: on coupled sparse/dense FEM/BEM linear systems of varying size.
#+NAME: chameleon
#+ATTR_LaTeX: :width 1\columnwidth
[[./figures/chameleon.svg]]

* Conclusion
:PROPERTIES:
:CUSTOM_ID: conclusion
:END:

We have evaluated the performance of the solvers branched to the
{{{test_FEMBEM}}} test suite on coupled sparse/dense FEM/BEM linear systems.
The solvers considered were {{{hmat-oss}}}, a sequential compressed dense direct
solver and {{{chameleon}}}, a multi-threaded non-compressed dense direct solver.

The comparison of sequential runs of {{{hmat-oss}}} and {{{chameleon}}} showed
an important positive impact of data compression on the time to solution. In
addition, the comparison of sequential and parallel runs of {{{chameleon}}} as
well as the computed parallel efficiency showed a considerable speed-up of the
parallel execution.

* Notes on reproducibility
:PROPERTIES:
:CUSTOM_ID: reproducibility
:END:

With the aim of keeping the experimental environment of the study reproducible,
we manage the associated software framework with the GNU Guix transactional
package manager cite:guix. Moreover, relying on the principles of literate
programming cite:Knuth84, we provide a full documentation on the construction
process of the experimental environment, the execution of benchmarks, the
collection and the visualization of results as well as on producing the final
manuscripts in a dedicated technical report associated with this study
cite:RT-EXAMPLE. A public companion contains all of the source code, guidelines
and other material required for reproducing the study:
[[https://gitlab.inria.fr/tuto-techno-guix-hpc/test_fembem/advanced-setup]],
archived on [[https://archive.softwareheritage.org/]] under the identifier
@@latex:\\@@ =swh:1:snp:79f450e0f43828f56f261d81b3e86aaab18362eb=.
